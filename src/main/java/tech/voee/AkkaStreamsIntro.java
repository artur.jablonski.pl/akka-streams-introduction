package tech.voee;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.stream.KillSwitch;
import akka.stream.KillSwitches;
import akka.stream.Materializer;
import akka.stream.UniqueKillSwitch;
import akka.stream.alpakka.file.DirectoryChange;
import akka.stream.alpakka.file.javadsl.Directory;
import akka.stream.alpakka.file.javadsl.DirectoryChangesSource;
import akka.stream.javadsl.BroadcastHub;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import tech.voee.support.Command;
import tech.voee.support.Command.AddWordCommand;
import tech.voee.support.Command.RemoveWordCommand;
import tech.voee.support.Server;
import tech.voee.support.WordDetector;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class AkkaStreamsIntro
{
  public static void main(String[] args)
    throws ExecutionException, InterruptedException
  {
    final ActorSystem system = ActorSystem.create("akka-streams-intro");
    final Materializer materializer = Materializer.createMaterializer(system);

    new Server(() -> webSocket(materializer))
      .startServer("localhost", 8080, system);

  }

  private static Flow<String, String, NotUsed> webSocket(
    Materializer materializer)
  {

    Pair<Source<String, NotUsed>, Sink<String, NotUsed>> sourceSinkPair =
      BroadcastHub.of(String.class).preMaterialize(materializer);

    Source<String, NotUsed> dynamicBcast = sourceSinkPair.first();

    Source<String, NotUsed> words =
      fromDirectory("words")
        .<Source<String, NotUsed>>statefulMapConcat(
          () -> {
            Map<String, KillSwitch> killSwitches =
              new HashMap<>();

            return
              c -> c.fold(
                add -> {
                  Pair<UniqueKillSwitch, Source<String, NotUsed>>
                    uniqueKillSwitchSourcePair =
                    dynamicBcast
                      .viaMat(
                        KillSwitches.single(),
                        Keep.right())
                      .via(wordDetector(
                        add.getWord()))
                      .preMaterialize(
                        materializer);

                  killSwitches
                    .put(add.getWord(),
                         uniqueKillSwitchSourcePair
                           .first());

                  return singletonList(
                    uniqueKillSwitchSourcePair
                      .second());
                },
                del -> {
                  killSwitches
                    .remove(del.getWord())
                    .shutdown();
                  return emptyList();
                }
              );
          }
        )
        .flatMapMerge(Integer.MAX_VALUE, s -> s);

    return Flow.fromSinkAndSourceCoupled(sourceSinkPair.second(), words);
  }

  private static Source<Command, NotUsed> fromDirectory(String directory)
  {
    return
      DirectoryChangesSource
        .create(Paths.get("", directory), Duration.ofSeconds(2), 128)
        .filter(p -> p.second() == DirectoryChange.Creation ||
                     p.second() == DirectoryChange.Deletion)
        .map(p -> p.second() == DirectoryChange.Creation ?
                  AddWordCommand.create(p.first().getFileName().toString()) :
                  RemoveWordCommand.create(p.first().getFileName().toString())
        )
        .prepend(
          Directory.ls(Paths.get("", directory))
                   .map(p -> AddWordCommand.create(p.getFileName().toString()))
        );
  }
  
  private static Flow<String, String, NotUsed> wordDetector(String word)
  {
    return
      Flow.of(String.class)
          .statefulMapConcat(
            () -> {
              WordDetector wd = new WordDetector(word);

              return
                s -> wd.isWordDetected(s.charAt(0)) ?
                     Collections.singleton(wd.getWord()) :
                     Collections.emptySet();
            }
          );
  }
}
