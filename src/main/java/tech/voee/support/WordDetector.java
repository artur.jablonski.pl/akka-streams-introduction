/*
 * Copyright (c) 2019 Artur Jabłoński
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tech.voee.support;

public class WordDetector
{
  private final String wordToDetect;

  //state
  private int currIndex;
  private boolean noMatch;

  public WordDetector(String wordToDetect)
  {
    this.currIndex = 0;
    this.wordToDetect = wordToDetect;
    this.noMatch = false;
  }

  public boolean isWordDetected(char currentCharacter)
  {

    if (Character.isWhitespace(currentCharacter))
      if (allCharsMatched()) {
        resetState();
        return true;
      } else {
        resetState();
        return false;
      }

    if (characterMatches(currentCharacter)) {
      currIndex++;
    } else {
      noMatch = true;
    }
    return false;

  }

  public String getWord()
  {
    return wordToDetect;
  }

  private boolean characterMatches(char currentCharacter)
  {
    return !noMatch &&
           currIndex < wordToDetect.length() &&
           wordToDetect.charAt(currIndex) == currentCharacter;

  }

  private boolean allCharsMatched()
  {
    return currIndex == wordToDetect.length() && !noMatch;
  }

  private void resetState()
  {
    currIndex = 0;
    noMatch = false;
  }
}
