package tech.voee.support;

import java.util.function.Function;

public abstract class Command
{
  private enum Type
  {
    ADD_WORD,
    REMOVE_WORD
  }

  protected Type commandType;
  protected String word;

  public String getWord()
  {
    return this.word;
  }

  public <T> T fold(
    Function<Command, T> addFunc,
    Function<Command, T> removeFunc)
  {
    switch (commandType) {
    case ADD_WORD:
      return addFunc.apply(this);
    case REMOVE_WORD:
      return removeFunc.apply(this);
    default:
      throw new IllegalStateException("kaboom!");
    }
  }

  public static class AddWordCommand extends Command
  {
    private AddWordCommand(String word)
    {
      this.commandType = Type.ADD_WORD;
      this.word = word;
    }

    public static AddWordCommand create(String word)
    {
      return new AddWordCommand(word);
    }
  }

  public static class RemoveWordCommand extends Command
  {
    private RemoveWordCommand(String word)
    {
      this.commandType = Type.REMOVE_WORD;
      this.word = word;
    }

    public static RemoveWordCommand create(String word)
    {
      return new RemoveWordCommand(word);
    }
  }
}
