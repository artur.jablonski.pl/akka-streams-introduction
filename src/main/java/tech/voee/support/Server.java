package tech.voee.support;

import akka.NotUsed;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.TextMessage;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.Route;
import akka.stream.javadsl.Flow;

import java.util.function.Supplier;

public class Server extends HttpApp
{

  private final Supplier<Flow<String, String, NotUsed>> webSocketFlowSupp;

  public Server(Supplier<Flow<String, String, NotUsed>> webSocketFlowSupp)
  {
    this.webSocketFlowSupp = webSocketFlowSupp;
  }

  @Override
  protected Route routes()
  {
    return concat(
      path("data",
           () -> {

             Flow<Message, Message, NotUsed> wsFlow =
               Flow.of(Message.class)
                   .map(m -> m.asTextMessage().getStrictText())
                   .via(webSocketFlowSupp.get())
                   .map(TextMessage::create);

             return handleWebSocketMessages(wsFlow);
           }
      ),
      get(() -> pathSingleSlash(() -> getFromResource("index.html"))
      )
    );
  }
}