package tech.voee.support;/*
 * Copyright (c) 2019 Artur Jabłoński
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class WordDetectorTest
{

  @Test
  public void happyPath()
  {
    WordDetector wordDetector = new WordDetector("dog");
    assertThat(wordDetector.isWordDetected('d')).isFalse();
    assertThat(wordDetector.isWordDetected('o')).isFalse();
    assertThat(wordDetector.isWordDetected('g')).isFalse();
    assertThat(wordDetector.isWordDetected(' ')).isTrue();

  }

  @Test
  public void wordWithPrefixFails()
  {
    WordDetector wordDetector = new WordDetector("dog");
    assertThat(wordDetector.isWordDetected('X')).isFalse();
    assertThat(wordDetector.isWordDetected('d')).isFalse();
    assertThat(wordDetector.isWordDetected('o')).isFalse();
    assertThat(wordDetector.isWordDetected('g')).isFalse();
    assertThat(wordDetector.isWordDetected(' ')).isFalse();

  }

  @Test
  public void wordWithSuffixFails()
  {
    WordDetector wordDetector = new WordDetector("dog");
    assertThat(wordDetector.isWordDetected('d')).isFalse();
    assertThat(wordDetector.isWordDetected('o')).isFalse();
    assertThat(wordDetector.isWordDetected('g')).isFalse();
    assertThat(wordDetector.isWordDetected('X')).isFalse();
    assertThat(wordDetector.isWordDetected(' ')).isFalse();

  }

  @Test
  public void differentWordFails()
  {
    WordDetector wordDetector = new WordDetector("dog");
    assertThat(wordDetector.isWordDetected('c')).isFalse();
    assertThat(wordDetector.isWordDetected('s')).isFalse();
    assertThat(wordDetector.isWordDetected('f')).isFalse();
    assertThat(wordDetector.isWordDetected('X')).isFalse();
    assertThat(wordDetector.isWordDetected(' ')).isFalse();

  }

  @Test
  public void wordAfterNewLineRecognized()
  {
    WordDetector wordDetector = new WordDetector("dog");
    assertThat(wordDetector.isWordDetected('\n')).isFalse();
    assertThat(wordDetector.isWordDetected('d')).isFalse();
    assertThat(wordDetector.isWordDetected('o')).isFalse();
    assertThat(wordDetector.isWordDetected('g')).isFalse();
    assertThat(wordDetector.isWordDetected(' ')).isTrue();

  }

}