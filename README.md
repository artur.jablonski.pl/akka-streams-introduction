# A practical introduction to Akka Streams (v2.6.0) with Java.

In this post I will go over some of _Akka Streams_ features. The various
possibilities that the API gives will be explored by building a relatively complex
sample application starting simple and building from there.
The core concepts behind _Akka Streams_ will be introduced on the way. 

## Sample application

What we will build here is a realtime as-you-type word detector application.


All the code is available in git repository [here](https://gitlab.com/artur.jablonski.pl/akka-streams-introduction). 
You can follow various stages of the application development by checking out appropriate commit.


To run the word detector from _stdin_ so that the words are detected as you type
and not when you hit _enter_, your
terminal needs to send characters one by one and not line by line which is default. 
If you use `bash` shell, you can execute `stty -icanon min 1` command before 
and that should sort it out. 


To compile and build execute `mvn clean package` which will build an "uber"
jar in _target_ directory. To execute go:
 
```
java -cp <path to target dir>/akka-streams-introduction-1.0-SNAPSHOT.jar tech.voee.AkkaStreamsIntro
```
 
## What is a (reactive) stream?

So before we go into any coding the basic question is "what is a stream"?

I think about a stream as an abstraction that allows to react to potentially multiple
values of some type that can be observed while the stream is active. Below is a visual
representation of a stream where the line represents time, the circles represent
values of certain type, the red x represents potential failure of the stream and the
vertical bar represents a normal termination of a stream:

![stream](img/stream.svg "fig 1. Stream") {.center}

The idea behind reactive streams programming is based on transformations
of such streams and _Akka Streams_ is one of APIs to do just that.
There are other APIs that implement the same reactive streams specification.


## Connecting shapes 

_Akka Streams_ API is based on idea of constructing a processing `Graph` out of 
`Shape`s. There are 3 basic `Shape`s available:

![source](img/source.svg?resize=200 "fig 2. Source") {.center}
![flow](img/flow.svg?resize=200 "fig 3. Flow") {.center}
![sink](img/sink.svg?resize=180 "fig 4. Sink") {.center}

Those are defined in terms of number of inputs and outputs. `Source` has no
inputs and one output, `Flow` has one input and one output and `Sink` has one
input and no outputs. 
The `T` `I` and `O` are types of data going through inputs/outputs
(these are the stream values as depicted by the circles in _fig 1_). The `M` is
called _materialized value_. I will mention what it is later.
Those 3 basic `Shape`s allow to create linear processing
pipelines like this:

![runnableGraph](img/runnablegraph.svg?resize=600 "fig 5. RunnableGraph") {.center}

Once the `Shape`s are connected so that there are no dangling inputs/outputs
left, what we get is an instance of `RunnableGraph` that can be executed. 


So let's do that and let's start to build our application:

```java
final ActorSystem system = ActorSystem.create("akka-streams-intro");
final Materializer materializer = Materializer.createMaterializer(system);

StreamConverters.fromInputStream(() -> System.in)
                .to(StreamConverters.fromOutputStream(() -> System.out))
                .run(materializer);
```

Here we took the `System.in` which is the _stdin_ of our process and converted
it to `Source` by using provided `StreamConverters.fromInputStream(...)` and then
we connected it with `to(...)` method to a `Sink` that we created from _stdout_
of our process. By doing that we obtained a `RunnableGraph` instance that we
could store and pass around as we please, but here we simply run it using the
`run(...)` method, which takes `Materializer` instance. This is where all the logic
of how to actually run the `Graph` lives. To obtain a `Materializer` we need
an `ActorSystem`, which basically is the runtime environment for the `Graph`.


If you run that code, what should happen is that every character that you type
on _stdin_ should be sent to _stdout_.

The data type that is flowing between our `Source` and `Sink` is `ByteString`,
but we want here a `String`, we will therefore transform the data by using
`map` operator. `map` simply takes lambda that for every element passed
returns another element:

```java
StreamConverters.fromInputStream(() -> System.in)
                .map(bs -> bs.decodeString(StandardCharsets.UTF_8));
```

You can also think of `map` and other linear operators as `Flow`s as they take
one input and have one output.  

We're not quite done here, just in case we are passed multiple characters
at the same time we want to make sure that there's only single character `String`s
flowing. For that we'll use `mapConcat` operator that similarly to `map` takes
one element at a time from upstream, but instead of returning a single instance
of transformed object it returns an `Iterable` of such elements. This means
that `mapConcat` for 1 element it gets may produce 0 (empty `Iterable`), 1 
(`Iterable` with just one element) or many elements (`Iterable` with many elements):

```java
StreamConverters.fromInputStream(() -> System.in)
                .map(bs -> bs.decodeString(StandardCharsets.UTF_8))
                .mapConcat(s -> Arrays.asList(s.split("")));
```

OK! We have now made sure we have only single character `String`s flowing.
Let's now plugin the actual word detecting logic, that has been implemented
in `WordDetector` class. The way this works is that you create an instance of 
this class configured with a word you want to detect, for example `new WordDetector("akka")`
and then feed it with characters using `isWordDetected(char character)` method, 
which will return `true` if the word has been detected and `false` otherwise. 
Therefore the object is *stateful* as it needs to remember the passed characters
that it has seen. To plugin such stateful objects we can use `statefulMapConcat`
operator:

```java
StreamConverters.fromInputStream(() -> System.in)
                .map(bs -> bs.decodeString(StandardCharsets.UTF_8))
                .mapConcat(s -> Arrays.asList(s.split("")));
                .statefulMapConcat(
                  () -> {
                    WordDetector detector = new WordDetector("akka");
                    return
                      s -> detector.isWordDetected(s.charAt(0)) ?
                           Collections.singletonList(detector.getWord()) :
                           Collections.emptyList();
                  }
                );
```

`statefulMapConcat` is similar to `mapConcat` in the way that it returns 
an `Iterable`, the difference is that the lambda passed to `statefulMapConcat` 
is called once per `Graph` execution. This is where you can set your state 
and then return the lambda that maps every upstream element to an `Iterable`.
That lambda can close over the state, which is exactly what we do here. 

So now when our `WordDetector` detects the word it will be passed downstream. 
We still want to output the word on _stdout_, but just so we see better what is
being typed in and what is being detected we will change the color of the word
in the terminal using a little utility and then our `Sink` expects `ByteString`
so we will do another transformation for that:

```java
StreamConverters.fromInputStream(() -> System.in)
                .map(bs -> bs.decodeString(StandardCharsets.UTF_8))
                .mapConcat(s -> Arrays.asList(s.split("")))
                .statefulMapConcat(
                  () -> {
                    WordDetector detector = new WordDetector("akka");
                    return
                      s -> detector.isWordDetected(s.charAt(0)) ?
                           Collections.singletonList(detector.getWord()) :
                           Collections.emptyList();
                  }
                )
                .map(TerminalStringColoring::colorBlue)
                .map(ByteString::fromString)
                .to(StreamConverters.fromOutputStream(() -> System.out))
                .run(materializer);
```

If you run this code, whenever you type "akka" followed by white character, 
you should see a blue "akka" word being printed on the terminal.

So that's all great, but let's tidy up a little bit the stream as it is getting
a bit convoluted and we're just starting.

## Composite shapes

To tidy up the stream we'll use the idea of composite shapes.

If you take a `Source` and attach >=1 `Flow`s to it and then look at what you
got by just looking at inputs and outputs, you still have `Source` shape, because
you have no inputs and you still have one output (which is the output of the 
last `Flow` that you attached):

![composeSource](img/composedsource.svg?resize=400 "fig 6. composite Source") {.center}

Similarly, if you connect many `Flow`s together, you will end up with a 
composite `Flow`:

![composeFlow](img/composedflow.svg?resize=400 "fig 7. composite Flow") {.center}
 
And if you take a `Sink` and prepend >=1 `Flow`s to it you will end up with
composite `Sink`:

![composeSink](img/composedsink.svg?resize=400 "fig 8. composite Sink") {.center}

So let's take that idea and refactor our original code:

```java
charsFromStdIn()
      .via(wordDetector("akka"))
      .to(toStdOutBlue())
      .run(materializer);
  

private static Source<String, CompletionStage<IOResult>> charsFromStdIn()
{
  return
    StreamConverters.fromInputStream(() -> System.in)
                    .map(bs -> bs.decodeString(StandardCharsets.UTF_8))
                    .mapConcat(s -> Arrays.asList(s.split("")));
}

private static Flow<String, String, NotUsed> wordDetector(String word)
{
  return
    Flow.of(String.class)
        .statefulMapConcat(
          () -> {
            WordDetector detector = new WordDetector(word);
             return
              s -> detector.isWordDetected(s.charAt(0)) ?
                   Collections.singletonList(detector.getWord()) :
                   Collections.emptyList();
          }
        );
}

private static Sink<String, NotUsed> toStdOutBlue()
{
  return
    Flow.of(String.class)
        .map(TerminalStringColoring::colorBlue)
        .map(ByteString::fromString)
        .to(StreamConverters.fromOutputStream(() -> System.out));
}
```

Other then use of `via` method to attach `Flow` to `Source` everything else 
is pretty much the same, though hopefully it's clearer now what each part of 
the stream is doing.

Let's now see how would we detect multiple words at the same time.

## Non linear topology with GraphDSL API  

If we wanted to detect multiple words at the same time, we would somehow need
to send the same characters coming from _stdin_ to different `WordDetector` 
instances each configured with different word we would like to detect. Unfortunately
the API we used so far cannot quite cut it. This is because it has been designed
with linear pipelines in mind, however good _Akka Streams_ folks gave us ninja
_GraphDSL_ API where you can create any topology you like including circular 
connections. 

Before we dive into it, let's introduce two more `Shape`s we'll use:

![fanOut](img/fanout.svg "fig 9. FanOut Shape") {.center}

`FanOut` is a junction `Shape` with one input and multiple outputs. We'll use it
for broadcasting the characters from _stdin_

![fanIn](img/fanin.svg "fig 10. FanIn Shape") {.center}

`FanIn` is a junction `Shape` with multiple inputs and one output. We'll use it
for merging the detections from `WordDetector` instances so that we can direct
them all to _stdout_.

This is a representation of what we'll build here showing two `WordDetector`s:

![composeComplex](img/composecomplex.svg "fig 11. Composite Flow with Broadcast and Merge") {.center}

No matter the complexity of the connections we can look at it as a `Flow` since
it has one input and one output. Let's see how this looks in the code:

```java
private static Flow<String, String, NotUsed> wordDetectors(String... words)
{
  return
    Flow.fromGraph(
      GraphDSL.create(
        b -> {
          UniformFanOutShape<String, String> bcast =
            b.add(Broadcast.create(words.length));
          UniformFanInShape<String, String> merge =
            b.add(Merge.create(words.length));

          for (String word : words) {
            b.from(bcast).via(b.add(wordDetector(word))).toFanIn(merge);
          }
          return FlowShape.of(bcast.in(), merge.out());
        }
      )
    );
}
```

We use `Flow.fromGraph(...)` to create our `Flow`. This method expects `Graph<FlowShape<I,O>, M>`
as its argument, we are providing it via `GraphDSL.create(...)` which is our entry
to the _GraphDSL_ API. The API is _builder_ pattern based. An instance of a builder
is passed to the lambda that is an argument to the `create` method. Inside 
that lambda we can add and connect all the elements that we need. First we create
`Broadcast` and `Merge` elements with as many outputs/inputs as we have words.
Then for each word we will connect one output of a `BroadCast` with a `WordDetector`
for that word and then connect that with an input of `Merge`. We then return
a `FlowShape` which input is the input of our `Broadcast` and output is the
output of the `Merge`. We can now plugin our composite `Flow` to our main stream
like this:

```java
charsFromStdIn()
      .via(wordDetectors("akka", "streams", "is", "awesome"))
      .to(toStdOutBlue())
      .run(materializer);
```

OK, this looks great, though it has a limitation in that we need to know 
the words we want to detect upfront before connecting the detectors.
Let's look how we can make this more dynamic.

## Dynamic topology with BroadcastHub

What we will look at now is ability of dynamically adding new `WordDetector`s while
our code is running.

In essence what we want is to be able to broadcast the characters from _stdin_
to detectors that are not known upfront, but can come and join the detection
at any time. _Akka Streams_ has a component just for such occasion. It's called
`BroadcastHub` and works like this:

![broadcastHube](img/broadcastHub.svg?resize=400 "fig 12. BroadcastHub") {.center}

`BroadcastHub` is a `Sink` that you can hook up with a `Source` which produces
the data to be dynamically broadcasted. Once you do that, you get a `RunnableGraph`
(of course) that you can run. This is where _materialized value_ come to play. 
In short, every `Source`, `Flow` and `Sink` may produce _materialized value_, which
is obtained once per `Graph` execution (once per calling `run` method to simplify 
it a bit for now). That value can be used for just anything, but in this case
it is a `Source` that can be then hooked up in another stream topologies multiple times.
Every time that `Source` instance is run in some `Graph` context it will emit the data that
is being emitted by the original `Source` connected to the `BroadcastHub`.

Let's see that in the code. First we'll hook up the `BroadcastHub` and run
the `Graph`:

```java
Source<String, NotUsed> dynamicBcast = 
  charsFromStdIn()
    .runWith(BroadcastHub.of(String.class), materializer);
```

We used `runWith(...)` because it will gives us the _materialized value_ of the `Sink`
as opposed to `run(...)` which gives us the _materialized value_ of the `Source`.


And now we can use that materialized `Source` in another stream:

```java
Source.from(Arrays.asList("akka", "streams", "is", "awesome"))
      .flatMapMerge(
        Integer.MAX_VALUE,
        w -> dynamicBcast.via(wordDetector(w))
      )
      .to(toStdOutBlue())
      .run(materializer);
```

Here we started the stream with a set of words and then used `flatMapMerge`
to connect the `dynamicBcast` with `WordDetector`s and to merge the detections.

It is still not that dynamic though as we start our stream with a fixed set of 
words, so let's change that.

## Project Alpakka

_Project Alpakka_ bridges the non reactive world of many technologies with _Akka
Streams_. We will use the filesystem part of it to monitor a folder on a filesystem
for created files:

```java
private static Source<String, NotUsed> wordsFromDir(String dir)
{
  return
    DirectoryChangesSource.create(Paths.get(dir), Duration.ofSeconds(3), 128)
                          .filter(
                            pair -> pair.second() == DirectoryChange.Creation)
                          .map(Pair::first)
                          .map(Path::getFileName)
                          .map(Path::toString);
}
```

Since there's no push notifications on filesystem, this particular `Source` is
polling for changes in directory with specified interval. We then filter only 
those events that means a file (or directory) has been created and then we use
couple of `map`s to get the actual file name. We can hook it up with our previous
stream:

```java
wordsFromDir("words")
      .flatMapMerge(
        Integer.MAX_VALUE,
        w -> dynamicBcast.via(wordDetector(w))
      )
      .to(toStdOutBlue())
      .run(materializer);
```

and now if we create a directory _words_ and run the code we can drive our
detectors from names of the files we create. If we want to also take into 
consideration files and directories that existed when we started our app, we
can tweak our `wordsFromDir` `Source` slightly:

```java
DirectoryChangesSource.create(Paths.get(dir), Duration.ofSeconds(3), 128)
                      .filter(
                        pair -> pair.second() == DirectoryChange.Creation)
                      .map(Pair::first)
                      .map(Path::getFileName)
                      .map(Path::toString)
                      .prepend(
                        Directory.ls(Paths.get(dir))
                                 .map(Path::getFileName)
                                 .map(Path::toString)
                      );
```

We use `prepend` operator to prepend names of any files and dirs that existed
in the same directory. 

We now have a truly dynamic setup where the words to detect are driven by filesystem.
That's great, how about removing words dynamically? Let's see how we can do that next.

## `KillSwitch` and `prematerialize(...)`

To remove the words dynamically we need first to change the driving stream slightly. 
First, we not only want the word itself but also an information if we want to add
or remove that word. For that I created `Command` class with two subClasses
`AddWordCommand` and `RemoveWordCommand`. We also want to hook into deletion
events coming from our filesystem `Source`:

```java
private static Source<Command, NotUsed> wordsFromDir(String dir)
{
  return
    DirectoryChangesSource.create(Paths.get(dir), Duration.ofSeconds(3), 128)
                          .filter(
                            pair ->
                              pair.second() == DirectoryChange.Creation ||
                              pair.second() == DirectoryChange.Deletion)
                          .map(
                            pair ->
                              pair.second() == DirectoryChange.Creation ?
                              Command.AddWordCommand.create(
                                pair.first().getFileName().toString()) :
                              Command.RemoveWordCommand
                                .create(pair.first().getFileName().toString())
                          )
                          .prepend(
                            Directory.ls(Paths.get(dir))
                                     .map(Path::getFileName)
                                     .map(Path::toString)
                                     .map(Command.AddWordCommand::create)
                          );
}
```

So let's go back to our main stream and update it so that it can handle 
`AddWordCommand`s:

```java
wordsFromDir("words")
      .mapConcat(
        c -> c.fold(
          ac -> Collections.singletonList(ac.getWord()),
          rc -> Collections.emptyList()
        )
      )
      .flatMapMerge(
        Integer.MAX_VALUE,
        w -> dynamicBcast.via(wordDetector(w))
      )
      .to(toStdOutBlue())
      .run(materializer);
```

Here I use `fold(..)` on `Command` class to either execute the first lambda
if `Command` is `AddWordCommand` and the second one if it's `RemoveWordCommand`.
This has nothing to do with _Akka Streams_, it's just me trying to be fancy
and not use `instanceof` to detect the type at runtime. 

Ok, so the code is back at what it was doing before, ignoring the `RemoveWordCommand`,
so let's change that. First when we handle `AddWordCommand` we need to save
some information so that we can use it later when `RemoveWordCommand` is issued
for the same word. We'll keep that state in a `Map` and the object of use for us here
is a `KillSwitch`:

```java
wordsFromDir("words")
    .<Source<String, NotUsed>>statefulMapConcat(
      () -> {
        Map<String, KillSwitch> killSwitches = new HashMap<>();
        return
          c -> c.fold(
            ac -> {
              Pair<UniqueKillSwitch, Source<String, NotUsed>>
                uniqueKillSwitchSourcePair =
                dynamicBcast.viaMat(KillSwitches.single(), Keep.right())
                            .via(wordDetector(ac.getWord()))
                            .preMaterialize(materializer);
              killSwitches
                .put(ac.getWord(), uniqueKillSwitchSourcePair.first());

              return
                Collections.singletonList(
                  uniqueKillSwitchSourcePair.second());
            },
            rc -> {
              killSwitches.remove(rc.getWord()).shutdown();
              return
                Collections.emptyList();
            }
          );
      }
    )
    .flatMapMerge(
      Integer.MAX_VALUE,
      s -> s
    )
    .to(toStdOutBlue())
    .run(materializer);

```  

There's few things going on here, so let's break it down. First we swapped from
`mapConcat` to `statefulMapConcat`, this is because we need to keep the state. 
That state we initialize as an empty `Map<String, KillSwitch>`. If we receive
`AddWordCommand` then the first lambda passed to `fold(...)` will be executed.
Here we take our `dynamicBcast` `Source` and attach a `KillSwitch` `Flow` to it
before connecting to `WordDetector` as before. That `KillSwitch` `Flow` materializes
an instance of `KillSwitch` that we can use to terminate that stream. The problem
is that we can't materialize that stream yet as this will be handled by the
`flatMapMerge` further down stream, this is why we use `prematerialize(...)`
instead which allows us to get materialized value without running the stream. 
What we get back is a `Pair` with prematerialized value and `Source` instance 
associated with it. We store first in our `Map` and pass the second down stream
where `flatMapMerge` will materialize and run it (internally).

When we receive `RemoveWordCommand` we simply take out the `KillSwitch` from the
`Map` and call `shutdown` on it. This will gracefully terminate the stream responsible
for detecting that particular word leaving the rest of the stream topology untouched.

If you run that code you should be now able to add and remove detected words
by creating and deleting files in monitored directory.

Now our application is pretty neat and the only thing that is missing to totally disrupt
the market of word detecting apps is to made it work via websocket. Let's look
at that next.

## Akka Http

_Akka Http_ provides both server and client side of HTTP communication, but what's
more important to us here is that it uses _Akka Streams_ abstractions all the way
through. 

This is extremely convenient to us as we already built our app around the same 
abstractions. So what does it take to plugin to a websocket? Well, all you need
to do is to provide a `Flow` that will consume the data from websocket and 
produce data to websocket, it can be visualised like this:

![websocket](img/websocket.svg "fig 12. Websocket Flow") {.center}

So let's refactor our code so that we have the processing `Flow` factored out,
so we can use it like this:

```java
charsFromStdIn()
      .via(procFlow(materializer))
      .to(toStdOutBlue())
      .run(materializer);

private static Flow<String, String, NotUsed> procFlow(Materializer materializer)
{
  //...
}
```

We will now implement the `procFlow(...)` method. For start we can't feed the 
_stdin_ directly to `BroadcastHub`. Instead we will prematerialize the `Sink`
directly. It's a similar approach we did with `KillSwitch` before:

```java
Pair<Source<String, NotUsed>, Sink<String, NotUsed>> sourceSinkPair =
      BroadcastHub.of(String.class)
                  .preMaterialize(materializer);

Source<String, NotUsed> dynamicBcast = sourceSinkPair.first();
```

What we get back from `preMaterialize(...)` is a pair of `Source` (our materialized 
dynamic broadcast) and a `Sink` associated with it (this is where we want to push
the data to).

Then we need to take the driving stream and instead of running it just capture
it in a variable:

```java
Source<String, NotUsed> words =
      wordsFromDir("words")
        .<Source<String, NotUsed>>statefulMapConcat(
          () -> {
            Map<String, KillSwitch> killSwitches = new HashMap<>();
            return
              c -> c.fold(
                ac -> {
                  Pair<UniqueKillSwitch, Source<String, NotUsed>>
                    uniqueKillSwitchSourcePair =
                    dynamicBcast.viaMat(KillSwitches.single(), Keep.right())
                                .via(wordDetector(ac.getWord()))
                                .preMaterialize(materializer);

                  killSwitches
                    .put(ac.getWord(), uniqueKillSwitchSourcePair.first());

                  return
                    Collections
                      .singletonList(uniqueKillSwitchSourcePair.second());
                },
                rc -> {
                  killSwitches.remove(rc.getWord())
                              .shutdown();
                  return
                    Collections.emptyList();
                }
              );
          }
        )
        .flatMapMerge(
          Integer.MAX_VALUE,
          s -> s
        );
```

that `Source` we captured will emit the detections. We need to now somehow
construct a `Flow` from `Sink` and `Source` and there's method just for that:

```java
return Flow.fromSinkAndSourceCoupled(sourceSinkPair.second(), words);
```

This may seem a bit counter intuitive at first, but if you look at it just from
the perspective of inputs and outputs, if you take a `Sink` and `Source` and
put them "back to back" you can look at them as composite `Flow`:

![flowFromSinkAndSource](img/flowFromSinkAndSource.svg?resize=400 "fig 13. Flow from Sink and Source") {.center}


Now we can indeed use the `procFlow` like this:

```java
charsFromStdIn()
      .via(procFlow(materializer))
      .to(toStdOutBlue())
      .run(materializer);
```

But more importantly we can take that same `Flow`, without changing a single
line of code and plug it in into _Akka Http_ server. 

There's a bit of plumbing required to associate given url with a websocket, 
but it's relatively simple and I prepared that in `Server` class that takes a
`Supplier<Flow<String, String, NotUsed>>` as a constructor argument. 
To start the server all we need to do is this:

```java
new Server(() -> procFlow(materializer)).startServer("localhost", 8080);
```

If all goes well, the server should start and if you fire up a browser and 
hit `http://localhost:8080` you should see a page with two text field. 
You can type in the right side text field and the detections will be sent
over to the left text field. There's a very simple _Javascript_ websocket
client handling the communication.

## Summary

In this post we built a fairly complex application based on _Akka Streams_.
As we went along we introduced and explained some concepts:

- Stream processing pipelines built as graphs.
- Graphs assembled out of shapes.
- Shapes available: Source, Flow, Sink, FanOut, FanIn
- Examples of Sources: StreamConverter.fromInputStream
- Examples of Flows: map, statefulMapConcat, etc
- Examples of Sinks: StreamConverter.fromOutputStream
- Composing shapes from other shapes for reusability/readibility 
- linear API and GraphDSL for non linear topologies
- Dynamic changes to pipelines: BroadcastHub
- Materialized values use and prematerialization: BroadcastHub and KillSwitch
- Stream based HTTP: websocket


## References
- Akka Streams documentation - https://doc.akka.io/docs/akka/current/stream/index.html
- Alpakka project - https://github.com/akka/alpakka
- Introduction to Reactive Streams for Java developers - https://www.lightbend.com/blog/reactive-streams-java
- Comparison of _Akka Streams_ with other reactive APIs - http://www.voee.tech/blog/akka-streams-rxjava2-project-reactor-api-comparison






 






